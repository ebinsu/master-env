# The project directory structure
locals {
  files = fileset(path.module, "../source/*.yaml")
  data  = [
  for f in local.files : {
    name : basename(f)
    content : file("${path.module}/${f}")
  }
  ]
}

resource "gitlab_repository_file" "apps" {
  for_each       = {for f in local.data : f.name => f}
  project        = gitlab_project.main.id
  branch         = gitlab_project.main.default_branch
  file_path      = "${var.target_path}/${each.value.name}"
  content        = each.value.content
  commit_message = "init flux cd"
}

resource "kubernetes_secret" "slack-url" {
  metadata {
    name      = "slack-url"
    namespace = "flux-system"
  }

  data = {
    address = data.sops_file.secrets.data["slack_url"]
  }
}

resource "gitlab_repository_file" "app-base-folders" {
  project        = gitlab_project.main.id
  branch         = gitlab_project.main.default_branch
  file_path      = "apps/base/README.md"
  content        = "base apps definition."
  commit_message = "init flux cd"
}

resource "gitlab_repository_file" "app-overlays-folders" {
  project        = gitlab_project.main.id
  branch         = gitlab_project.main.default_branch
  file_path      = "apps/overlays/README.md"
  content        = "apps diff env config."
  commit_message = "init flux cd"
}

resource "gitlab_repository_file" "infrastructure-source-folders" {
  project        = gitlab_project.main.id
  branch         = gitlab_project.main.default_branch
  file_path      = "infrastructure/source/README.md"
  content        = "HelmRepository, ImageRepository definition."
  commit_message = "init flux cd"
}