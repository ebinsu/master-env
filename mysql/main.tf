terraform {
  required_providers {
    mysql = {
      source  = "bangau1/mysql"
      version = "1.10.4"
    }
  }
}

locals {
  dbSecret = jsondecode(file("./db-secret.json"))
  dbConfig = jsondecode(file("./db-config.json"))
}

# Configure the MySQL provider
provider "mysql" {
  endpoint = local.dbConfig.db_host
  username = local.dbConfig.db_username
  password = local.dbSecret.db_password
}

# Create a Database
resource "mysql_database" "db" {
  for_each = {for db_name in local.dbConfig.db_names : db_name => db_name}
  name     = each.value
}

resource "mysql_user" "mysql_user" {
  for_each           = {for db_user in local.dbSecret.db_users : db_user.user => db_user}
  user               = each.value.user
  host               = "%"
  plaintext_password = each.value.password

  depends_on = [mysql_database.db]
}

resource "mysql_grant" "mysql_grant" {
  for_each   = {for user_grant in local.dbConfig.db_user_grants : join("-", [user_grant.user, user_grant.db, user_grant.table]) => user_grant}
  user       = each.value.user
  host       = "%"
  database   = each.value.db
  privileges = each.value.privileges

  depends_on = [mysql_user.mysql_user]
}